#  -----------------------------------------------------------------------------
#  MOOG PROPRIETARY AND CONFIDENTIAL INFORMATION
#  -----------------------------------------------------------------------------
#  This technical Data/Drawing/Document contains information that is proprietary
#  to, and is the express property of Moog Inc., or Moog Inc. subsidiaries
#  except as expressly granted by contract or by operation of law and is
#  restricted to use by only Moog employees and other persons authorized in
#  writing by Moog or as expressly granted by contract or by operation of law.
#  No portion of this Data/Drawing/Document shall be reproduced or disclosed or
#  copied or furnished in whole or in part to others or used by others for any
#  purpose whatsoever except as specifically authorized in writing by Moog Inc.
#  or Moog Inc. subsidiary.
# 
#  -----------------------------------------------------------------------------
#  COPYRIGHT NOTICE
# -----------------------------------------------------------------------------
#  Copyright: Moog Inc. 2016
# 
#  -----------------------------------------------------------------------------
#  FILE INFORMATION
#  -----------------------------------------------------------------------------
#  File Name    : DOCXReportGenerator.py
#  -----------------------------------------------------------------------------
#  NOTES
#  -----------------------------------------------------------------------------
#
#  -----------------------------------------------------------------------------
#  REVISION HISTORY:
#    Date      By    Description of Change
#  -------- -------- -----------------------------------------------------------
#  20160121 cfritz   Initial
#  20160128 cfritz   Finished implementation of create_pdf_report
#  20160224 cfritz   Rewored into create_docx_report for docx file creation

import glob
from lxml import etree
import os
import sys
import re
import win32com.client
import sys
import zipfile
import shutil


## Parse all test reports within a directory and create a summary XML report.
#  Also create a table of test configurations, where a test configuration is 
#  defined as a unique set of header parameters (excluding calibration and date/time info)
#  This function takes a parent directory name, parses all report XML files within,
#  and creates an XML output summary at the specified output path and an XML configurations
#  file at the other path.
#  XSL files are also created in the same location as each of the table XML files 
#  for styling and should be kept with the output XML files for viewing.
#
#  Written C. Fritz, 8/27/15
#
# @param path                the path to the top level report directory
# @param summary_output      the output XML file to create containing the report summary
# @param configs_output      the output XML file to create containing the collection of configurations
# @param include_testcases   create a column for the test case name in the summary table output (default = False)
#
def createXMLSummaryConfigTables(path, summary_output, configs_output, include_testcases=False):
    STYLESHEET = '''<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="tests">
<html>
  <head>
  <style TYPE="text/css">
  
   body
{
    font-family: sans-serif;
    margin-left: 5%;
    margin-right: 5%;
}

table.header
{
    margin: auto;
    border-width: 1;
    border-color: black;
    font-size: 0.8em;
    width: 50%;
    border-style: double;
    border-spacing: 0;
    border-collapse: collapse;
    margin-bottom: 20;
}
table.main
{
    width: 100%;
    border-width: 1;
    border-color: black;
    border-style: solid;
    border-spacing: 0;
    border-collapse: collapse;
    margin-bottom: 20;
}


.tsc
{
    border-width: 1;
    border-style: solid;
    border-color: #CCCCCC;
    border-collapse: collapse;
    padding: 3;
}

td.bggreen
{
    color: darkgreen;
    background-color: lightgreen;
    border-collapse: collapse;
}

td.bgred
{
    color: white;
    background-color: red;
    border-collapse: collapse;
}

.tsc.expand
{
    width: 100%;
}

.tsc.end
{
    width: 10%;
}

.tsc.middle
{
    width: 80%;
}

.tsc.half
{
    width: 50%;
}

td.bgyellow
{
    color: black;
    background-color: yellow;
    border-collapse: collapse;
}

td.det
{
    border: none;
    vertical-align: top;
}


td.right
{
    text-align: right;
}

td.center
{
    text-align: center;
}

td.title
{
    background-color: #000088;
    text-align: center;
    font-weight: bold;
    color: #CCCCCC
}

td.desc
{
    background-color: #AAAAAA;
    text-align: right;
    vertical-align: top;
    font-weight: bold;
    white-space: nowrap;
    width: 1%;
}

td.val
{
    width: auto;
    border-width: 1;
    border-style: solid;
    border-color: #CCCCCC;
    border-collapse: collapse;
}

td.valwhite
{
    width: auto;
    border-width: 1;
    border-style: solid;
    border-color: #CCCCCC;
    border-collapse: collapse;
    color : #FFFFFF;
}

td.columnhead
{
    font-weight: bold;
    text-align: center;
    width: auto;
    border-width: 1;
    border-style: solid;
    border-color: #CCCCCC;
    border-collapse: collapse;
}
  </style>
  </head>
  <body>
      <h1>3 Test Results</h1>
      <h2>3.1 Test Summary Table</h2>
      <br />
      <table class="tsc expand">
      <td colspan="7" class="title">Test Case Result Summary</td>
      <tr>
        <td class="columnhead">Test#</td>
        <td class="columnhead">Case#</td>
        <td class="columnhead">Test Name</td>
        <td class="columnhead">Test Config.</td>
        <td class="columnhead">Test Result</td>
        <td class="columnhead">PR</td>
        <td class="columnhead">Link to Report</td>
      </tr>
        <xsl:for-each select="//test">
          <tr>
            <td class="val half"><xsl:value-of select="test_num"/></td>
            <td class="val center"><xsl:value-of select="test_case"/></td>
            <td class="val center"><xsl:value-of select="test_name"/></td>
            <td class="val center"><xsl:value-of select="test_config"/></td>
            <xsl:choose>
              <xsl:when test="test_result = 'PASSED'">
                <td class="tsc center bggreen"><xsl:value-of select="test_result"/></td>
               </xsl:when>
              <xsl:when test="test_result = 'INVALID'">
                <td class="tsc center bgyellow"><xsl:value-of select="test_result"/></td>
               </xsl:when>
              <xsl:when test="test_result = 'FAILED'">
                <td class="tsc center bgred"><xsl:value-of select="test_result"/></td>
               </xsl:when>
              <xsl:otherwise>
                <td class="tsc center bgyellow"><xsl:value-of select="test_result"/></td>
               </xsl:otherwise>
             </xsl:choose>
             
           <td class="valwhite center">placeholder</td>
           <td class="val center"><a href="{test_link}">Link</a></td>
           </tr>
         </xsl:for-each>
       </table>
    </body>
</html>

</xsl:template>

</xsl:stylesheet>

'''

    STYLESHEET2 = '''<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="configs">
<html>
  <head>
  <style TYPE="text/css">
  
   body
{
    font-family: sans-serif;
    margin-left: 5%;
    margin-right: 5%;
}


table.header
{
    margin: auto;
    border-width: 1;
    border-color: black;
    font-size: 0.8em;
    width: 50%;
    border-style: double;
    border-spacing: 0;
    border-collapse: collapse;
    margin-bottom: 20;
}
table.main
{
    width: 100%;
    border-width: 1;
    border-color: black;
    border-style: solid;
    border-spacing: 0;
    border-collapse: collapse;
    margin-bottom: 20;
}


.tsc
{
    border-width: 1;
    border-style: solid;
    border-color: #CCCCCC;
    border-collapse: collapse;
    padding: 3;
}

td.bggreen
{
    color: darkgreen;
    background-color: lightgreen;
    border-collapse: collapse;
}

td.bgred
{
    color: white;
    background-color: red;
    border-collapse: collapse;
}

.tsc.expand
{
    width: 100%;
}

.tsc.end
{
    width: 10%;
}

.tsc.middle
{
    width: 80%;
}

.tsc.half
{
    width: 50%;
}

td.bgyellow
{
    color: black;
    background-color: yellow;
    border-collapse: collapse;
}

td.det
{
    border: none;
    vertical-align: top;
}


td.right
{
    text-align: right;
}

td.center
{
    text-align: center;
}

td.title
{
    background-color: #000088;
    text-align: center;
    font-weight: bold;
    color: #CCCCCC
}

td.desc
{
    text-align: left;
    vertical-align: top;
    font-weight: bold;
    white-space: nowrap;
    width: 1%;
    border-color: #CCCCCC;
    border-style: solid;
    border-width: 1;
}

td.val
{
    width: auto;
    border-width: 1;
    border-style: solid;
    border-color: #CCCCCC;
    border-collapse: collapse;
}

td.columnhead
{
    background-color: #AAAAAA;
    font-weight: bold;
    text-align: center;
    width: auto;
    border-width: 1;
    border-style: solid;
    border-color: #CCCCCC;
    border-collapse: collapse;
}
  </style>
  </head>
  <body>
    <h2>3.2 Test Stand Configurations</h2>
    <br />
      <table class="tsc expand">
      <td colspan="2" class="title">Test Configurations Summary</td>
      <tr>
        <td class="columnhead">Parameter Name</td>
        <xsl:for-each select="//config">
            <td class="columnhead">Config <xsl:value-of select="@id"/></td>
        </xsl:for-each>
      </tr>
      <tr><td class="desc">SSW SW Version</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="sswsw"/></td></xsl:for-each></tr>
      <tr><td class="desc">SSW Number</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="sswsn"/></td></xsl:for-each></tr>
      <tr><td class="desc">SSW Scripts</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="sswscripts"/></td></xsl:for-each></tr>
      <tr><td class="desc">LIB Aileron</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="libaileron"/></td></xsl:for-each></tr>
      <tr><td class="desc">LOB Aileron</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="lobaileron"/></td></xsl:for-each></tr>
      <tr><td class="desc">RIB Aileron</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="ribaileron"/></td></xsl:for-each></tr>
      <tr><td class="desc">ROB Aileron</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="robaileron"/></td></xsl:for-each></tr>
      <tr><td class="desc">LIB Elevator</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="libelevator"/></td></xsl:for-each></tr>
      <tr><td class="desc">LOB Elevator</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="lobelevator"/></td></xsl:for-each></tr>
      <tr><td class="desc">RIB Elevator</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="ribelevator"/></td></xsl:for-each></tr>
      <tr><td class="desc">ROB Elevator</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="robelevator"/></td></xsl:for-each></tr>
      <tr><td class="desc">Upper Rudder</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="upperrudder"/></td></xsl:for-each></tr>
      <tr><td class="desc">Lower Rudder</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="lowerrudder"/></td></xsl:for-each></tr>
      <tr><td class="desc">LIB MFS3</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="libmfs3"/></td></xsl:for-each></tr>
      <tr><td class="desc">LIB MFS4</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="libmfs4"/></td></xsl:for-each></tr>
      <tr><td class="desc">LOB MFS5</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="lobmfs5"/></td></xsl:for-each></tr>
      <tr><td class="desc">LOB MFS6</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="lobmfs6"/></td></xsl:for-each></tr>
      <tr><td class="desc">L GSA1</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="lgsa1"/></td></xsl:for-each></tr>
      <tr><td class="desc">L GSA2</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="lgsa2"/></td></xsl:for-each></tr>
      <tr><td class="desc">RIB MFS3</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="ribmfs3"/></td></xsl:for-each></tr>
      <tr><td class="desc">RIB MFS4</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="ribmfs4"/></td></xsl:for-each></tr>
      <tr><td class="desc">ROB MFS5</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="robmfs5"/></td></xsl:for-each></tr>
      <tr><td class="desc">ROB MFS6</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="robmfs6"/></td></xsl:for-each></tr>
      <tr><td class="desc">R GSA1</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="rgsa1"/></td></xsl:for-each></tr>
      <tr><td class="desc">R GSA2</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="rgsa2"/></td></xsl:for-each></tr>
      <tr><td class="desc">IB GSA CM</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="ibgsacm"/></td></xsl:for-each></tr>
      <tr><td class="desc">OB GSA CM</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="obgsacm"/></td></xsl:for-each></tr>
      <tr><td class="desc">FCC1</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="fcc1"/></td></xsl:for-each></tr>
      <tr><td class="desc">FCC2</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="fcc2"/></td></xsl:for-each></tr>
      <tr><td class="desc">FCC3</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="fcc3"/></td></xsl:for-each></tr>
      <tr><td class="desc">FCC1 CSCI Version</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="csci1ver"/></td></xsl:for-each></tr>
      <tr><td class="desc">FCC2 CSCI Version</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="csci2ver"/></td></xsl:for-each></tr>
      <tr><td class="desc">FCC3 CSCI Version</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="csci3ver"/></td></xsl:for-each></tr>
      <tr><td class="desc">FCC1 FPGA Version</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="fpga1ver"/></td></xsl:for-each></tr>
      <tr><td class="desc">FCC2 FPGA Version</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="fpga2ver"/></td></xsl:for-each></tr>
      <tr><td class="desc">FCC3 FPGA Version</td><xsl:for-each select="//config"><td class="val"><xsl:value-of select="fpga3ver"/></td></xsl:for-each></tr>
       </table>
    </body>
</html>

</xsl:template>

</xsl:stylesheet>

'''



    if not include_testcases:
        STYLESHEET = STYLESHEET.replace('<td class="val center"><xsl:value-of select="test_case"/></td>', '') 
        STYLESHEET = STYLESHEET.replace('<td class="columnhead">Test Case</td>', '') 
        STYLESHEET = STYLESHEET.replace('colspan="5"', 'colspan="4"') 
    tests = etree.Element("tests")
    configs = []
    tagOrderedList = [('sswsw', 'SSW SW Version'), ('sswsn', 'SSW Number'), ('sswscripts', 'SSW Scripts'),\
                      ('libaileron', 'LIB Aileron'), ('lobaileron', 'LOB Aileron'), ('ribaileron', 'RIB Aileron'),\
                      ('robaileron', 'ROB Aileron)'), ('libelevator', 'LIB Elevator'), ('lobelevator', 'LOB Elevator'),\
                      ('ribelevator', 'RIB Elevator'), ('robelevator', 'ROB Elevator'), ('upperrudder', 'Upper Rudder'),\
                      ('lowerrudder', 'Lower Rudder'), ('libmfs3', 'LIB MFS3'), ('libmfs4', 'LIB MFS4'),\
                      ('lobmfs5', 'LOB MFS5'), ('lobmfs6', 'LOB MFS6'), ('lgsa1', 'L GSA1'), ('lgsa2', 'L GSA2'),\
                      ('ribmfs3','RIB MFS3'), ('ribmfs4', 'RIB MFS4'), ('robmfs5', 'ROB MFS5'), ('robmfs6', 'ROB MFS6'),\
                      ('rgsa1', 'R GSA1'), ('rgsa2', 'R GSA2'), ('ibgsacm', 'IB GSA CM'), ('obgsacm','OB GSA CM'),\
                      ('fcc1', 'FCC1'), ('fcc2', 'FCC2'), ('fcc3', 'FCC3'), ('csci1ver', 'FCC1 CSCI Version'),\
                      ('csci2ver', 'FCC2 CSCI Version'), ('csci3ver', 'FCC3 CSCI Version'), ('fpga1ver', 'FCC1 FPGA Version'),\
                      ('fpga2ver', 'FCC2 FPGA Version'), ('fpga3ver', 'FCC3 FPGA Version')]
    
    includeTags = [x for (x,_) in tagOrderedList]
    pathtuples = os.walk(path)
    
    for dirtuple in pathtuples: 
        foundXML = False
        for filename in dirtuple[2]:
            if '.xml' in filename and os.path.exists(dirtuple[0] + '\\xml\\ssw_Log.css'):
                foundXML = True
                xm = etree.XML(open(dirtuple[0] + '\\' + filename).read())
                namestr = xm.find('header/title').text
                numstr = namestr.split('_')[0]
                casestr = re.findall('Case(.?)_', namestr)[0]
                config = {}
                for element in xm.findall('./header/*'):
                    if element.tag in includeTags:
                        config[element.tag] = element.text
                if config not in configs:
                    configs.append(config)
                configNum = configs.index(config)
                for element in xm.findall('./log/tc'):
                    id = element.find('./id')
                    result = element.find('./result')
                    idstr = ''
                    resstr = ''
                    if id is not None:
                        idstr = id.text
                    if result is not None:
                        resstr = result.text
                    test = etree.SubElement(tests, "test")
                    testnum = etree.SubElement(test, "test_num")
                    testnum.text = numstr
                    if include_testcases:
                        testcase = etree.SubElement(test, "test_case")
                        testcase.text = casestr
                    testname = etree.SubElement(test, "test_name")
                    tempstr = ''
                    for st in namestr.split('_')[1:]:
                        tempstr += '_' + st
                    testname.text = tempstr[1:]
                    testresult = etree.SubElement(test, "test_result")
                    testresult.text = resstr
                    testlink = etree.SubElement(test, "test_link")
                    testlink.text = filename
                    testconfig = etree.SubElement(test, "test_config")
                    testconfig.text = str(configNum+1)
                    testpr = etree.SubElement(test, "test_pr")
                    testpr.text = 'placeholder'    
        if not foundXML and 'Case' in dirtuple[0].split('\\')[-1]:
            #Add entry to summary table without link.
            dirname = dirtuple[0].split('\\')[-1]
            numstr = dirname.split('_')[0]
            casestr = re.findall('Case_*([0-9]+)', dirname)[0]
            
            test = etree.SubElement(tests, "test")
            testnum = etree.SubElement(test, "test_num")
            testnum.text = numstr
            testnum = etree.SubElement(test, "test_case")
            testnum.text = casestr
            testname = etree.SubElement(test, "test_name")
            testname.text = dirname
            
            
            
    #Create summary xml table file and corresponding stylesheet
    of = open(summary_output, 'w')
    xmlwrite = etree.tostring(tests, pretty_print=True)
    outputRaw = summary_output[:-4]
    stylestr = '<?xml version="1.0" encoding="ASCII"?>\n<?xml-stylesheet type="text/xsl" href="%s.xsl"?>\n' % (outputRaw,)
    of.write(stylestr + xmlwrite)
    of.close()
    
    of = open(outputRaw + '.xsl', 'w')
    of.write(STYLESHEET)
    of.close()
    
    #Create configs xml table file and corresponding stylesheet
    configsxm = etree.Element("configs")
    for nc, c in enumerate(configs):
        config = etree.SubElement(configsxm, "config")
        config.attrib['id'] = str(nc+1) 
        for (key, namestr) in tagOrderedList:
            if key in c:
                valstr = c[key]
            else:
                valstr = ''
            item = etree.SubElement(config, key)
            item.text = valstr

    of = open(configs_output, 'w')
    xmlwrite = etree.tostring(configsxm, pretty_print=True)
    outputRaw = configs_output[:-4]
    stylestr = '<?xml version="1.0" encoding="ASCII"?>\n<?xml-stylesheet type="text/xsl" href="%s.xsl"?>\n' % (outputRaw,)
    of.write(stylestr + xmlwrite)
    of.close()
    of = open(outputRaw + '.xsl', 'w')
    STYLESHEET2 = STYLESHEET2.replace('colspan="2"', 'colspan="%s"' % (str(len(configs)+1),))
    of.write(STYLESHEET2)
    of.close()

TOC_HTML = '''
<html>
<body>
<h1>Table of Contents</h1>
<ul>
INTROSTRING
<li>TESTNUM. Test Results
    <ul>
        <li><a href="summary.html">3.1 Test Summary Table</a></li>
        <li><a href="configs.html">3.2 Test Stand Configurations</a></li>
    </ul></li>
<li><a href="appendix.html">Appendix A: Test Reports</a></li>
</ul>
<body>
<html>

'''

    
## Create combined DOCX reports, given a top level directory which contains XML reports
#  that already exist. The provided basepath should be a directory under which 
#  each test result subdirectory is contained. An output filename should be provided.
#  The DOCX output will be in a Moog PDM format with a coverpage and header/footer on each page.
#  A summary DOCX report will be generated with tables listing the reports and test 
#  configurations.
#  Also, appendix DOCX reports will be generated as specified, based on ranges of test numbers.
#
#  DEPENDS ON: 
#     > Win32 extensions 
#     > Zipfile         (native)
#     > Shutil          (native)
#
#  Written C. Fritz, 1/28/16 - 2/24/16 cfritz@moog.com
#
# @param basepath            the path to the top level report directory
# @param outfilename         the desired filename of the PDF output file
# @param appencides          list of appendix filenames and ranges of test numbers to include in each appendix
#                            test numbers must be >= the first number and < the second to be included
#                            e.g. {'AppendixA' : [10020,10021], 'AppendixB' : [10021, 10022]
#                            the appendix files will be created in the same directory as the summary file.
#    
def create_docx_report(basepath, outfilename, appendices={}):
    
    createXMLSummaryConfigTables(basepath, 'summary.xml', 'configs.xml', True)
    summary_dom = etree.parse('summary.xml')
    summary_xslt = etree.parse('summary.xsl')
    transform = etree.XSLT(summary_xslt)
    summary_newdom = transform(summary_dom)
    open('summary.html', 'w').write(etree.tostring(summary_newdom))
    
    config_dom = etree.parse('configs.xml')
    config_xslt = etree.parse('configs.xsl')
    transform = etree.XSLT(config_xslt)
    config_newdom = transform(config_dom)
    open('configs.html', 'w').write(etree.tostring(config_newdom))
    
    for appendix in appendices:
        appendix_filename = ''.join(x + '\\' for x in outfilename.split('\\')[:-1]) + appendix + '.docx'
        pathtuples = os.walk(basepath)
        html_files = []
        for dirtuple in pathtuples: 
            for filename in dirtuple[2]:
                if '.xml' in filename and os.path.exists(dirtuple[0] + '\\xml\\ssw_Log.css'):
                    testnum = int(dirtuple[0].split('\\')[-2])
                    if testnum >= appendices[appendix][0] and testnum < appendices[appendix][1]:
                        css = open(dirtuple[0] + '\\xml\\ssw_Log.css', 'r')
                        #Parse this into a structure so that we can convert the HTML to inline
                        res = re.findall('(.+?)\n{\n([.\s\S]+?)}', css.read())
                        css_struct = {res[x][0]:res[x][1].replace('\n','').replace('\t', '').replace('    ', ' ') for x in range(len(res))}
                        css.close()
                        dom = etree.parse(dirtuple[0] + '\\' + filename)
                        xslt = etree.parse(dirtuple[0] + '\\xml\\ssw_Log.xsl')
                        transform = etree.XSLT(xslt)
                        newdom = transform(dom)
                        tmp_html_fname = dirtuple[0] + '\\' + filename.replace('.xml', '.html')
                        html_files.append(tmp_html_fname)
                        tmp_html = open(tmp_html_fname, 'w')
                        newhtml = etree.tostring(newdom).replace('<img class="entry scaled"', '<img width=400 height=320')
                        res = re.findall('<([^>]+?) .*?class="([^>]+?)"', newhtml)

                        for tag, classstr in res:
                            if tag != 'link':
                                inlinecss = ''
                                rawclass = []
                                for c in classstr.split(' '):
                                    try:
                                        inlinecss += css_struct[tag + '.' + c]
                                    except:
                                        rawclass.append(c)
                                rawstr = ''
                                for rc in rawclass:
                                    try:
                                        rawstr += '.' + rc
                                        inlinecss += css_struct[rawstr]
                                    except:
                                        pass
                            
                                res = re.findall('<%s [^>]*?class="%s">' % (tag, classstr), newhtml)
                                for val in res:
                                    val2 = val.replace('class="%s"' % (classstr,), 'style="%s"' % (inlinecss,))
                                    newhtml = newhtml.replace(val, val2)
                        tmp_html.write(newhtml)
       
        word_files = []
        word = win32com.client.Dispatch('Word.Application')
        new_doc = word.Documents.Add()
        for html_file in [os.getcwd() + '\\CoverPage.docx'] + html_files:
            doc = word.Documents.Add(html_file)
            word.Selection.WholeStory()
            word.Selection.Copy()
            doc.Close()
            word.Selection.Paste()
            
        new_doc.SaveAs2(appendix_filename, FileFormat=16) #docx, https://msdn.microsoft.com/en-us/library/ff839952.aspx
        new_doc.Close()
        
        summary_doc = word.Documents.Add()
        for filename in [os.getcwd() + '\\CoverPageTOC.docx', os.getcwd() + '\\summary.html', os.getcwd() + '\\configs.html']:
            doc = word.Documents.Add(filename)
            word.Selection.WholeStory()
            word.Selection.Copy()
            doc.Close()
            word.Selection.Paste()
        
        summary_doc.SaveAs2(outfilename, FileFormat=16)
        summary_doc.Close()
        
        
        os.mkdir('zipout')
       #Add header and footer manually:
        with zipfile.ZipFile(appendix_filename, 'r') as z:
            z.extractall(os.getcwd() + '\\zipout')
        topcwd = os.getcwd()
        os.chdir('.\\zipout')
        os.remove('.\\[Content_Types].xml')
        shutil.copy('..\\header_footer_files\\[Content_Types].xml','[Content_Types].xml')
        os.chdir('.\\word')
        #os.mkdir('.\\media')
        #shutil.copy(topcwd + '\\header_footer_files\\word\\media\\image1.png', '.\\media\\image1.png')
        shutil.copy(topcwd + '\\header_footer_files\\word\\footer2.xml', '.\\footer2.xml')
        shutil.copy(topcwd + '\\header_footer_files\\word\\header2.xml', '.\\header2.xml')
        shutil.copy(topcwd + '\\header_footer_files\\word\\footer3.xml', '.\\footer3.xml')
        shutil.copy(topcwd + '\\header_footer_files\\word\\header3.xml', '.\\header3.xml')
        os.chdir('.\\_rels')
        shutil.copy(topcwd + '\\header_footer_files\\word\\_rels\\footer2.xml.rels', 'footer2.xml.rels')
        relfile = open('document.xml.rels', 'r')
        relstr = relfile.read()
        relfile.close()
        relstr = relstr.split('</Relationships>')[0] + \
            '<Relationship Target="footer3.xml" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/footer"\
            Id="footerFirst"/><Relationship Target="header3.xml" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/header"\
            Id="headerFirst"/><Relationship Target="footer2.xml" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/footer" \
            Id="footerID"/><Relationship Target="header2.xml" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/header" \
            Id="headerID"/></Relationships>'
        relfile = open('document.xml.rels', 'w')
        relfile.write(relstr)
        relfile.close()
        os.chdir('..')
        docfile = open('document.xml', 'r')
        docstr = docfile.read()
        docfile.close()
        docparts = docstr.split('<w:sectPr')
        tagend = docparts[1].find('>')
        docstr = docparts[0] +  '<w:sectPr ' + docparts[1][:tagend+1] + '<w:footerReference r:id="footerID" w:type="default"/>\
                                 <w:headerReference r:id="headerID" w:type="default"/><w:titlePg/>' + docparts[1][tagend+1:]
        docfile = open('document.xml', 'w')
        docfile.write(docstr)
        docfile.close()
        
        os.chdir(topcwd)
        os.chdir('.\\zipout')
        zipf = zipfile.ZipFile(appendix_filename, 'w')
        for root, dirs, files in os.walk('.'):
            for file in files:
                zipf.write(os.path.join(root, file))
        zipf.close()
        os.chdir('..')
        shutil.rmtree('.\\zipout')
    
    


if __name__ == '__main__':
    create_docx_report('C:\\Users\\cfritz\\Documents\\Embraer\\HW_Results',\
                       'C:\\Users\\cfritz\\Documents\\Embraer\\DOCXReportGeneration\\Outputs\\HW_Results2.docx', \
                        appendices={'AppendixA' : [10020,10021], 'AppendixB' : [10021,10022]})

    